﻿using CodeAssignment.Infrastructure.Interfaces;
using CodeAssignment.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodeAssignment.Controllers
{
    public class WebApiController : Controller
    {
        private readonly IExampleService<Result> service = null;

        public WebApiController()
        {
            service = Injector.injectorContainer.GetInstance<IExampleService<Result>>();
        }

        public ActionResult GetJsonData()
        {
            List<Result> model = service.GetItemList().ToList();
            return Json(new { results = model }, JsonRequestBehavior.AllowGet);
        }
    }
}