﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeAssignment.Infrastructure.Interfaces;
using CodeAssignment.Services;
using CodeAssignment.Infrastructure.Models;

namespace CodeAssignment
{
    public static class Injector
    {
        // 1. Create a new Simple Injector container
        static readonly Container container = new Container();

        public static void Init()
        {
            // 2. Configure the container (register)
            container.Register<IExampleService<Result>, ExampleFromJSONService>();
       
            // 3. Verify your configuration
            container.Verify();
        }


        public static Container injectorContainer { get { return container; } }
    }
}