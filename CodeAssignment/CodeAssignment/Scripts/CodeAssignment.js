﻿function displayId(data) {
    return $("<span />").text(data.i);
}
function displayName(data) {
    return $("<span />").text(data.n);
}
function displayCategory(data) {
    return $("<span />").text(data.stn);;
}
function showUrl(data) {
    return $("<a />")
        .attr("href", "https://beta.twa.nl/nl" + data.url)
        .text("https://beta.twa.nl/nl" + data.url);
}function displayIdName(data) {
    return $("<span />").text(data.i + " " + data.n);}function displayCategoryAndCountryInfo(data) {
    var countryAndCode = $("<span />").addClass('fr_lable').text(data.c+ " " + data.cc);
    return $("<div/>").text(data.stn).append(countryAndCode);
}function GetSales(_jsonUrl) {
    $.getJSON(_jsonUrl, function (data) {
        var ul = $("<ul/>")
            .addClass('my-new-list');

        //for mapping using ko or angular nothing was said about it in task, so i just noticed it 
        $.each(data.results, function (key, item) {
            var categoryAndCountry = displayCategoryAndCountryInfo(item);
            var idName = displayIdName(item);
            var link = showUrl(item);
            var li = $('<li />')
                .attr('id', key)
                .append(categoryAndCountry)
                .append(idName)
                .append(link);
            ul.append(li);

        });
        
        ul.appendTo("#list-of-sales");
    });
}