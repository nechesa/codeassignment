﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeAssignment.Infrastructure.Models;
using CodeAssignment.Infrastructure.Interfaces;
using System.Web.Configuration;
using System.IO;
using System.Reflection;

namespace CodeAssignment.Services
{
    public class ExampleFromJSONService : BaseService, IExampleService<Result>
    {
        public Result GetItem(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Result> GetItemList()
        {
            try
            {
                string fileName = WebConfigurationManager.AppSettings["JSONFilePath"];
                string fileDir = AppDomain.CurrentDomain.BaseDirectory;
                string jsonData = String.Empty;


                using (StreamReader sr = new StreamReader(Path.Combine(fileDir, fileName)))
                {
                    jsonData = sr.ReadToEnd();
                }

                ExampleModel model = deserializeJSON<ExampleModel>(jsonData);
                if (model != null && model.results != null && model.results.Any())
                {
                    return model.results;
                }
            }
            catch (Exception ex)
            {
                //To do: protocol errors 
            }

            return Enumerable.Empty<Result>();
        }


        public void CreateItem(Result item)
        {
            throw new NotImplementedException();
        }

        public void DeleteItem(int id)
        {
            throw new NotImplementedException();
        }

       

        public void UpdateItem(Result item)
        {
            throw new NotImplementedException();
        }
    }
}
