﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssignment.Services
{
    public class BaseService
    {
        public T deserializeJSON<T>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(data);
            }
            catch
            {
                return default(T);
            }
        }

        public string serializeToJSON(object data)
        {
            try
            {
                return JsonConvert.SerializeObject(data);
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
