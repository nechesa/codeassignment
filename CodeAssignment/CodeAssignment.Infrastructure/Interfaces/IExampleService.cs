﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssignment.Infrastructure.Interfaces
{
    public interface IExampleService<T> 
        where T : class
    {
        IEnumerable<T> GetItemList(); // получение всех объектов
        T GetItem(int id); // получение одного объекта по id
        void CreateItem(T item); // создание объекта
        void UpdateItem(T item); // обновление объекта
        void DeleteItem(int id); // удаление объекта по id
    }
}
