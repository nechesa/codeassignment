﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeAssignment.Infrastructure.Models
{
    public class Result
    {
        public string at { get; set; }
        public int bi { get; set; }
        public int cat { get; set; }
        public int ct { get; set; }
        public string bd { get; set; }
        public bool dpdtz { get; set; }
        public object hi { get; set; }
        public int ii { get; set; }
        public bool icat { get; set; }
        public bool it { get; set; }
        public int nol { get; set; }
        public string stn { get; set; }
        public string shd { get; set; }
        public bool scd { get; set; }
        public string st { get; set; }
        public string t { get; set; }
        public object ti { get; set; }
        public bool up { get; set; }
        public string url { get; set; }
        public int vl { get; set; }
        public bool ctl { get; set; }
        public bool glr { get; set; }
        public int nr { get; set; }
        public string n { get; set; }
        public string sn { get; set; }
        public string d { get; set; }
        public string c { get; set; }
        public string cc { get; set; }
        public string cr { get; set; }
        public int m { get; set; }
        public int mv { get; set; }
        public int v { get; set; }
        public int sd { get; set; }
        public int cd { get; set; }
        public bool ic { get; set; }
        public bool ip { get; set; }
        public int sti { get; set; }
        public bool sbi { get; set; }
        public int i { get; set; }
        public object rh { get; set; }
    }

    public class ExampleModel
    {
        public int id { get; set; }
        public int offset { get; set; }
        public IList<Result> results { get; set; }
        public int total { get; set; }
    }
}
